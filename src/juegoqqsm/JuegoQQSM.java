
package juegoqqsm;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Estudiante;
import modelo.Materia;
import modelo.Menu;
import modelo.Termino;
import modelo.Paralelo;
import modelo.Termino;
public class JuegoQQSM {

    ArrayList<Estudiante> estudiantes;
    ArrayList<Termino> terminos = new ArrayList<>();
    ArrayList<Materia> materias = new ArrayList<>();
    
    public JuegoQQSM(){
        estudiantes = new ArrayList<>();
    }
    public static void main(String[] args) {
        
        JuegoQQSM juego =new JuegoQQSM();
        //juego.leerArchivo();
        //juego.cargarArchivoTermino();
        //juego.leerArchivo(); // no lo haya por que esta comentado
        Scanner entrada = new Scanner(System.in);
        //System.out.println(juego.estudiantes);
        //System.out.println(juego.terminos);
        
        //Creacion de arraylist para mostrar
      
        System.out.println("\tQuien Quiere ser millonario, Sean todos bienvenidos\n");
        juego.menuInicial();
    }
    
    public void menuInicial(){
        int opc;
        Scanner entrada = new Scanner(System.in);
        System.out.println("MENÚ PRINCIPAL "
                + "\nEscoja una opcion: "
                + "\n1. Configuraciones"
                + "\n2. Nuevo Juego"
                + "\n3. Reporte"
                + "\n4. Salir");
        
        opc = entrada.nextInt();
        switch (opc){
                case 1:configuraciones();
                case 2:nuevojuego();
                case 3:reporte();
                case 4:break;
        }
    } 
    
    //Este va a leer Archivo solo para estudiantes
        public void leerArchivo() {
        BufferedReader csvReader = null;
        try {
            String ruta = "src/archivos/CCPG1005-2-2019-2.csv"; //ruta del archivo que se va a leer
            csvReader = new BufferedReader(new FileReader(ruta));
            String fila = csvReader.readLine();//escapar cabecera de archivo
            while ((fila = csvReader.readLine()) != null) { //iterar en el contenido del archivo
                String[] data = fila.split(",");
                estudiantes.add(new Estudiante(data[0], data[1], data[2])); //crear objeto y agregar a lista

            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(JuegoQQSM.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(JuegoQQSM.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                csvReader.close();
            } catch (IOException ex) {
                Logger.getLogger(JuegoQQSM.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    //Este sobrescribira Archivo solo estuidante
    
    public void actualizarArchivo() {
        FileWriter writer = null;
        try {
            String ruta = "src/archivos/CCPG1005-2-2019-2.csv"; //ruta del archivo que se va a leer
            writer = new FileWriter(ruta);
            for (Estudiante est : estudiantes) {
                writer.write(est.getMatricula() + "," + est.getNombre() + "," + est.getEmail() + System.lineSeparator());
            }
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(JuegoQQSM.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                writer.close();
            } catch (IOException ex) {
                Logger.getLogger(JuegoQQSM.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }
    
    public void configuraciones(){
        int opc;
        Scanner sc = new Scanner(System.in);
        System.out.println("Bienvenido a las configuraciones del juego");
        System.out.println("Escoja una opcion: "
                + "\n1.Administrar términos académicos"
                + "\n2.Administrar materias y paralelos"
                + "\n3.Administrar preguntas"
                + "\n4.Salir");
        
        opc = sc.nextInt();
        switch (opc){
            case 1:
                menuTerminos();
            case 2:
                menuMaterias();
            case 3: 
                menuPreguntas();
            case 4:
                menuInicial();
        }
        
    }
    
    public void cargarArchivoTermino() {
        BufferedReader csvReader = null;
        try {
            String ruta = "src/archivos/terminos.csv"; //ruta del archivo que se va a leer
            csvReader = new BufferedReader(new FileReader(ruta));
            String fila = csvReader.readLine();//escapar cabecera de archivo
            while ((fila = csvReader.readLine()) != null) { //iterar en el contenido del archivo
                String[] data = fila.split("\n");
                terminos.add(new Termino(data[0])); //crear objeto y agregar a lista

            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(JuegoQQSM.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(JuegoQQSM.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                csvReader.close();
            } catch (IOException ex) {
                Logger.getLogger(JuegoQQSM.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void actualizarArchivoTermino() {
        FileWriter writer = null;
        try {
            String ruta = "src/archivos/terminos.csv"; //ruta del archivo que se va a leer
            writer = new FileWriter(ruta);
            writer.write("Anio-Termino\n");
            for (Termino ter : terminos) {
                writer.write(ter.getTermino()+ System.lineSeparator());
            }
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(JuegoQQSM.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                writer.close();
            } catch (IOException ex) {
                Logger.getLogger(JuegoQQSM.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void menuTerminos(){
        int numero;
        int anio;
        int indice;
        int opc;
        String terminoEscogido;
        Scanner sc = new Scanner(System.in);
   
        System.out.println(terminos);
        System.out.println("Escoja una opcion: "
                + "\n1.Ingresar termino"
                + "\n2.Eliminar termino"
                + "\n3.Configurar termino para juego"
                + "\n4.Salir");
        
        opc = sc.nextInt();
        switch(opc){
            case 1:
                System.out.println("1. Ingresar un término nuevo");
                System.out.print("Año >> ");
                anio=sc.nextInt();
                System.out.print("Número del término >> ");
                numero=sc.nextInt();
                terminos.add(new Termino(anio,numero)); 
                //actualizarArchivoTermino(); 
                menuTerminos();
            case 2:
                System.out.println("2. Eliminar término");
                System.out.println("Escribe el término que quieres eliminar:");
                sc.nextLine();
                terminoEscogido=sc.nextLine();
                //Obtencion de indice 
                for(int i = 0 ; i <terminos.size();i++){
                    if (terminoEscogido.equalsIgnoreCase(terminos.get(i).getTermino())){
                        terminos.remove(i);
                        System.out.println("Termino Elimnado de manera Correcta");
                    }
                }
                //actualizarArchivoTermino(); 
                menuTerminos();
            case 3:
                System.out.println("3. Configurar término para juego");
                System.out.println("Ingrese termino y estudiante para el nuevo juego:");
                System.out.println("Escribe el término que escogeras para el juego:");
                terminoEscogido=sc.nextLine();
                break;
            case 4:
                configuraciones();
        }}
    
    //2. Administrar materias y paralelos
    public void menuMaterias(){
        String nombre;
        String codigo;
        String termino;
        int numparalelo;
        int nivel;
        Paralelo paraleloEliminar;
        Scanner sc = new Scanner(System.in);
        
        System.out.println(materias);
                
        System.out.println("Escoja una opcion: "
                + "\n1.Ingresar materia"
                + "\n2.Editar materia"
                + "\n3.Desactivar materia"
                + "\n4.Agregar paralelo"
                + "\n5.Eliminar paralelo"
                + "\n6.Salir");
        
        int opc = sc.nextInt();
        switch(opc){
            case 1:
                System.out.println("1. Ingresar materia");
                System.out.println("Nombre de la materia: ");
                nombre=sc.nextLine();
                sc.next();
                System.out.println("Código de la materia: ");
                codigo=sc.nextLine();
                sc.next();
                System.out.println("Cantidad de niveles: ");
                nivel=sc.nextInt();
                materias.add(new Materia(nivel,nombre,codigo));
                menuMaterias();
            case 2:
                System.out.println("2. Editar materia");
                System.out.println("Código de la materia: ");
                codigo=sc.nextLine();
                menuMaterias();
            case 3:
                System.out.println("3. Desactivar materia");
                menuMaterias();
            case 4:
                System.out.println("4. Agregar paralelo");
                System.out.println("Materia:");
                nombre=sc.nextLine();
                System.out.println("Término:");
                termino=sc.next();
                System.out.println("Número del paralelo:");
                numparalelo=sc.nextInt();
                Paralelo objParalelo=new Paralelo(nombre,termino,numparalelo);
                objParalelo.AgregarParalelo(objParalelo);
                menuMaterias();
            case 5:
                System.out.println("5. Eliminar paralelo");
                System.out.println("Escribe el paralelo que quieres eliminar:");
                //paraleloEliminar=sc.nextLine();
                //objParalelo.EliminarParalelo(paraleloEliminar);
                menuMaterias();
            case 6:
                configuraciones();
        }
    }
        
    public void menuPreguntas(){
        int opc=0;
        String codigoMateria;
        Scanner sc = new Scanner(System.in);
        System.out.println("Escoja una opcion: \n1.Visualizar preguntas\n2.Agregar pregunta\n3.Eliminar paralelo\n4.Salir");
        opc = sc.nextInt();

        System.out.println("Bienvenido a las configuraciones del juego\n Se le mostrara una lista con terminos por defecto observela ytome las debidas acciones "
                + "");
        Scanner entrada = new Scanner(System.in);
        System.out.println(terminos);
        System.out.println("Escoja una opcion porfavor:\n1.Ingresar termino\n2.Eliminar termino\n3.Configurar termino para juego");
        for(Termino apuntador: terminos){
            System.out.println(apuntador.toString());
        }
        switch(opc){
            case 1:
                System.out.println("1.Visualizar preguntas");
                System.out.println("Código de la materia: ");
                codigoMateria=sc.nextLine();
                //Falta que muestre las preguntas
                break;
            case 2:
                System.out.println("1.Agregar pregunta");
                break;
            case 3:
                System.out.println("1.Eliminar paralelo");
                break;
            case 4:
                configuraciones();
                break;
        }
    }

    public void nuevojuego(){
        String Materia;
        int Paralelo;
        int Nivel;
        Scanner sc = new Scanner(System.in);
        System.out.println("Nuevo juego");
        System.out.println("Nombre de la materia:");
        Materia=sc.nextLine();
        System.out.println("Número del paralelo: ");
        Paralelo=sc.nextInt();
        System.out.println("Número de niveles:");
        Nivel=sc.nextInt();
        System.out.println("Presione una tecla para empezar");
        
    }
    
    public void reporte(){
        String Termino;
        String codigoParalelo;
        int Paralelo;
        Scanner sc = new Scanner(System.in);
        System.out.println("Término:");
        Termino=sc.nextLine();
        System.out.println("Código del paralelo:");
        codigoParalelo=sc.nextLine();
        System.out.println("Número del paralelo: ");
        Paralelo=sc.nextInt();
        
    }
    
}


