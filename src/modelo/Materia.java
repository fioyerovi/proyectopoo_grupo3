/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;
/**
 *
 * @author fioye
 */
public class Materia {
    private int nivel;
    private String nombre;
    private String codigo;
    private ArrayList paralelos;

    public Materia(int nivel, String nombre, String codigo) {
        this.nivel = nivel;
        this.nombre = nombre;
        this.codigo = codigo;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public ArrayList getParalelos() {
        return paralelos;
    }

    public void setParalelos(ArrayList paralelos) {
        this.paralelos = paralelos;
    }
}