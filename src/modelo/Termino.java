/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;

/**
 *
 * @author fioye
 */
public class Termino {
    //Variables
    private int anio;
    private int numero;
    private String termino;
    private String terminoEscogido;
    private ArrayList<String> listaTerminos = new ArrayList<>();
    

    //Constructor
    public Termino(String termino) {
        this.termino = termino;
    }

    public Termino(int anio, int numero) {
        this.anio = anio;
        this.numero = numero;
        this.termino = anio+"-"+numero;
    }

    public void IngresarTermino(int anio, int numero){
        termino=anio+"-"+numero;
        //Lista de términos
        listaTerminos.add(termino); //Término nuevo
        
    }
    //2. Eliminar término
    public void EliminarTermino(String termino){
       
        listaTerminos.remove(termino);
        System.out.println(listaTerminos);
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getTermino() {
        return termino;
    }

    public void setTermino(String termino) {
        this.termino = termino;
    }

    public String getTerminoEscogido() {
        return terminoEscogido;
    }

    public void setTerminoEscogido(String terminoEscogido) {
        this.terminoEscogido = terminoEscogido;
    }

    public ArrayList<String> getListaTerminos() {
        return listaTerminos;
    }

    public void setListaTerminos(ArrayList<String> listaTerminos) {
        this.listaTerminos = listaTerminos;
    }
    
    @Override
    public String toString() {
        return "Termino: " + termino;
    }

}

   
