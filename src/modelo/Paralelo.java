/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;
import java.util.ArrayList;

/**
 *
 * @author fioye
 */
public class Paralelo {
    private String termino;
    private String materia;
    private int numeroParalelo;
    private Paralelo paralelo;
    private ArrayList<Paralelo> listaParalelos = new ArrayList<>();
    //Getters y Setters

    public String getTermino() {
        return termino;
    }

    public void setTermino(String termino) {
        this.termino = termino;
    }

    public String getMateria() {
        return materia;
    }

    public void setMateria(String materia) {
        this.materia = materia;
    }

    public int getNumeroParalelo() {
        return numeroParalelo;
    }

    public void setNumeroParalelo(int numeroParalelo) {
        this.numeroParalelo = numeroParalelo;
    }
    
    //4. Agregar Paralelo
    public Paralelo(String termino, String materia, int numeroParalelo) {
        this.termino = termino;
        this.materia = materia;
        this.numeroParalelo = numeroParalelo;
    
    }
    public void AgregarParalelo(Paralelo paralelo){
        //Lista de paralelos
        
        listaParalelos.add(paralelo); //Paralelo nuevo
    
    }
    public void EliminarTermino(Paralelo paralelo){
        System.out.println(listaParalelos);
        listaParalelos.remove(paralelo);
    }   
}
