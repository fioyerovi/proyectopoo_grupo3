/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author fioye
 */
public class Estudiante {
    
    private String matricula;
    private String nombre; //se debe poner el nombre completo
    private String email;

    public Estudiante(String matricula, String nombreCompleto, String email) {
        this.matricula = matricula;
        this.nombre = nombre;
        this.email = email;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombre = nombreCompleto;
    }

    public String getEmail() {
        return email;
    }

    public void setCorreo(String correo) {
        this.email = email;
    }
    
    
    
    
}
