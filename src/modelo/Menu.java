
package modelo;

import java.util.Scanner;

public class Menu {
    
    public static void menuInicial(){
        int opc;
        Scanner entrada = new Scanner(System.in);
        System.out.println("MENÚ PRINCIPAL "
                + "\nEscoja una opcion: "
                + "\n1. Configuraciones"
                + "\n2. Nuevo Juego"
                + "\n3. Reporte"
                + "\n4. Salir");
        
        opc = entrada.nextInt();
        switch (opc){
                //case 1:configuraciones();
                //case 2:nuevojuego();
                //case 3:reporte();
                case 4:break;
        }
    } 

    public void configuraciones(){
        int opc;
        Scanner sc = new Scanner(System.in);
        System.out.println("Bienvenido a las configuraciones del juego");
        System.out.println("Escoja una opcion: "
                + "\n1.Administrar términos académicos"
                + "\n2.Administrar materias y paralelos"
                + "\n3.Administrar preguntas"
                + "\n4.Salir");
        
        opc = sc.nextInt();
        switch (opc){
            case 1:
                //menuTerminos();
            case 2:
                //menuMaterias();
            case 3: 
                //menuPreguntas();
            case 4:
                menuInicial();
        }
        
    }

    
}
